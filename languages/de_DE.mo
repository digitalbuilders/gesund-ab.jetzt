��          �   %   �      P     Q  	   c     m     u     �     �     �  c   �  =        N     ]     l  %   p     �     �     �     �     �  \   �     0     <     B  $   F  )   k     �  �  �     u     �  	   �     �  #   �     �     �  m   �  J   X     �     �     �  ,   �     �       
   
          )  d   1     �     �     �  /   �     �      	                              	                                                         
                                      Add widgets here. Back home Comment Comment navigation Comments are closed. Footer Footer  It looks like nothing was found at this location. Maybe go back home or see the latest posts below. Leave a Comment<span class="screen-reader-text"> on %s</span> Newer Comments Older Comments One Oops! That page can&rsquo;t be found. Pages: Primary Read more text Search Results for: %s Sidebar Sorry, but nothing matched your search terms. Please try again with some different keywords. Tagged %1$s Three Two Your comment is awaiting moderation. comments title%$1s comment %$1s comments comments titleOne comment Project-Id-Version: GreatMag
PO-Revision-Date: 2020-03-04 22:06+0100
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
X-Poedit-Basepath: ..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_n:1,2;_n_noop:1,2;_c;esc_attr__;esc_attr_e;esc_html__;esc_html_e;esc_html_x:1,2c;esc_attr_x:1,2c
Last-Translator: 
Language: de_DE
X-Poedit-SearchPath-0: .
 Füge hier Widgets hinzu. Zurück Kommentar Navigation Kommentare Kann nicht mehr kommentiert werden. Footer Footer  Hier konnte nichts gefunden werden. Gehe vielleicht zurück oder siehe die letzten Beiträge weiter unten an. Hinterlasse einen Kommentar<span class="screen-reader-text"> auf %s</span> Neuere Kommentare Ältere Kommentare Eins Uuups. Die Seite kann nicht gefunden werden. Seiten: Primär Mehr lesen Such-Ergebnisse: %s Sidebar Sorry, aber deine Suche hat keine Ergebnisse geliefert. Versuche es bitte mit anderen Suchbegriffen. Beitrag in %1$s  Drei Zwei Dein Kommentar muss noch freigeschaltet werden. %$1s Kommentar %$1s Kommentare Ein Kommentar 