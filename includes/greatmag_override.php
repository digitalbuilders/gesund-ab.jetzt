<?php
/** Modified from GreatMag theme (https://athemes.com/theme/greatmag/), so this code is released under the GPLv2+. */

/**
 * We have to kick out some hooks (and reset them later) that were set by GreatMag
 * to reset texts in the footer and the header.
 */
add_action( 'init' , 'remove_functions' , 15 );
 function remove_functions() {
   remove_action( 'greatmag_footer', 'greatmag_footer_bottom', 8 );
   remove_action( 'greatmag_before_header', 'greatmag_top_bar' );
}

/**
 * Adjusted Footer credits and menu.
 */
function greatmag_child_footer_bottom() {
  ?>
    <div class="row bottom-footer" id="bottom-footer">
      <div class="container">
        <?php greatmag_child_custom_credits(); ?>

        <nav id="footer-navigation" class="footer-navigation footer-menu-box">
          <?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'nav nav-pills footer-menu', 'depth' => 1, 'fallback_cb' => false ) ); ?>
        </nav>
      </div>
    </div>
  <?php
}

add_action( 'greatmag_footer', 'greatmag_child_footer_bottom', 8 );

/**
 * Adjusted Credits
 */
function greatmag_child_custom_credits() {
    echo '<div class="site-info">';
	    
    echo '© 2020 Viktoria Thomas - betrieben bei <a href="https://digitalbuilders.eu" target="_new">DIGTAL BUILDERS GmbH</a>';
	
    echo '<span class="sep"> | </span>';
    printf( __( 'Powered by %s', 'greatmag' ), 'Free Software' );
	
    echo '</div>';
}


/**
 * Adjusted Top bar
 */
function greatmag_child_top_bar() {

  $hide_search_icon = get_theme_mod( 'hide_search_icon', 0 );

  ?>
    <?php if( $hide_search_icon == 0 ) { ?>
    <div class="top-search-form row">
      <?php get_search_form(); ?>
    </div>
    <?php } ?>
    <div class="top-header row">
      <div class="container">
        <div class="row">
          <div class="col-sm-8">
            <?php greatmag_child_latest_news(); ?>
          </div>
          <div class="col-sm-4 auth-social">
            <?php greatmag_social_login(); ?>
          </div>
        </div>
      </div>
    </div>
  <?php
}

add_action( 'greatmag_before_header', 'greatmag_child_top_bar');

/**
 * "Latest news" in header.
 */
function greatmag_child_latest_news() {
  $number   = get_theme_mod('latest_news_number', 5);
  $ln_title   = get_theme_mod('latest_news_title', __( 'Latest news', 'greatmag') );
  /*$query = new WP_Query( array(
    'posts_per_page'      => $number,
    'no_found_rows'       => true,
    'post_status'         => 'publish',
    'ignore_sticky_posts' => true
  ) );*/
  ?>

  <div class="media breaking-news">
    <div class="media-left">
      <div class="bnews-label"><a href="/tag/blog"><?php echo esc_html($ln_title); ?></a></div>
    </div>
    <div class="media-body">
      <div class="bnews-ticker">
      </div>
    </div>
  </div>

  <?php
}


/**
 * Override from GreatMag Theme to increase logo size
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function greatmag_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on GreatMag, use a find and replace
	 * to change 'greatmag' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'greatmag', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'greatmag-extra-small', 100, 70, true );
	add_image_size( 'greatmag-small', 270, 180, true );
	add_image_size( 'greatmag-medium', 380, 250, true );
	add_image_size( 'greatmag-featured-a', 1200, 850, true );
	add_image_size( 'greatmag-featured-b', 790, 535, true );
	add_image_size( 'greatmag-featured-c', 600, 425, true );
	add_image_size( 'greatmag-single', 710 );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'greatmag' ),
		'footer'  => esc_html__( 'Footer', 'greatmag' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'greatmag_custom_background_args', array(
		'default-color' => 'eeeeee',
		'default-image' => '',
	) ) );

	//Logo support
	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 800,
		'flex-height' => true,
	) );

	//Video post format support
	add_theme_support( 'post-formats', array( 'video' ) );
}

?>
