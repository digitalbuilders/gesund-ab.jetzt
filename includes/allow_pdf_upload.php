<?php

/** Allow PDF upload */
function gabj_mime_types($mime_types){
    $mime_types['svg'] = 'application/pdf';
    return $mime_types;
}
add_filter('upload_mimes', 'gabj_mime_types', 1, 1);

?>
