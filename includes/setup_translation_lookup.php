<?php

/** Add path to load modified (parent) language file. */
function gabj_setup_translation_lookup() {
    load_child_theme_textdomain( 'greatmag', get_stylesheet_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'gabj_setup_translation_lookup' );

?>
