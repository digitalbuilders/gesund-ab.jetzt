# gesund-ab.jetzt Wordpress Child Theme

Child theme of the wordpress [GreatMag theme](https://athemes.com/theme/greatmag/), which is released under the GPLv3.

## Implemented Features and Changes

### Changes to GreatMag

In [includes/greatmag_override.php](includes/greatmag_override.php) footer and header are modified to 
  - show custom credits
  - remove the latest post slider in the header
  - increase the logo size in the header
.

### Other Changes

- Allow Upload of PDF files (in [`includes/allow_pdf_upload.php`](includes/allow_pdf_upload.php))

### Translation

Some strings are translated into German, using `poedit` and the original `*.pot` file. For some `_x()` and `_nx()` strings, new entry were added to the `*.pot` file. The files are located in `languages/`. In order to use these translated files from this child theme, `gabj_slug_setup_for_languages` is implemented in [`include/setup_translation_lookup.php`](include/setup_translation_lookup.php).

### Documentation of other features

- Using [pods](https://pods.io/) for a custom Setting page that fills the "Termine" Widget on the right sidebar.
- The Blog shows all Posts with the "Blog" Tag (had to be done in order to use some GreatMag Layout).
- Frontend-styles added via Customizer.


## License

The original code within this repository is licensed under the AGPLv3+ (meaning, AGPLv3 or any later version). The license text is contained in the file [LICENSE.txt](LICENSE.txt).

Parts that were copied from the parent theme (["GreatMag"](https://athemes.com/theme/greatmag/)) are licensed under the GPLv2+.

All original work within this repository is Copyright 2020, DIGITAL BUILDERS GmbH ([https://digitalbuilders.eu](https://digitalbuilders.eu)).
